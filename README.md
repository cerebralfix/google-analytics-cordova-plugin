CF-google-analytics-cordova-plugin
==================================

Cordova (PhoneGap) 3.0+ Plugin to connect to Google's native Universal Analytics SDK

#Changes made by CerebralFix
* Exposed set functionality on tracker configurations

Note: all changes made are iOS specific, therefore the Android portion of this code is not up to equivalent spec

Prerequisites:
* A Cordova 3.0+ project for iOS and/or Android
* A Mobile App property through the Google Analytics Admin Console
* (Android) Google Play Services SDK installed via [Android SDK Manager](https://developer.android.com/sdk/installing/adding-packages.html)

#Installing

This plugin follows the Cordova 3.0+ plugin spec, so it can be installed through the Cordova CLI in your existing Cordova project:
```bash
cordova plugin add https://github.com/danwilson/google-analytics-plugin.git
```

If you are not using the CLI, follow the steps in the section [Installing Without the CLI](#nocli)

#Basic JavaScript Usage
In your 'deviceready' handler, set up your Analytics tracker:
* `window.analytics.startTrackerWithId('UA-XXXX-YY')` where UA-XXXX-YY is your Google Analytics Mobile App property

To track a Screen (PageView):
* `window.analytics.trackView('Screen Title')`

To track an Event:
* `window.analytics.trackEvent('Category', 'Action', 'Label', Value)` Label and Value are optional, Value is numeric

To track an Exception:
* `window.analytics.trackException('Description', Fatal)` where Fatal is boolean

To track User Timing (App Speed):
* `window.analytics.trackTiming('Category', IntervalInMilliseconds, 'Variable', 'Label')` where IntervalInMilliseconds is numeric

To add a Transaction (Ecommerce)
* `window.analytics.addTransaction('ID', 'Affiliation', Revenue, Tax, Shipping, 'Currency Code')` where Revenue, Tax, and Shipping are numeric

To add a Transaction Item (Ecommerce)
* `window.analytics.addTransactionItem('ID', 'Name', 'SKU', 'Category', Price, Quantity, 'Currency Code')` where Price and Quantity are numeric

To add a Custom Dimension
* `window.analytics.addCustomDimension('Key', 'Value', success, error)`

To set a UserId:
* `window.analytics.setUserId('my-user-id')`

To set a Tracker field:
* `window.analytics.setTrackerField('Field', 'Value')` where Field is one of (AppName, AppId, AppInstallerId, AppVersion) and value is a corresponding string value

To enable verbose logging:
* `window.analytics.debugMode()`

To enable/disable automatic reporting of uncaught exceptions
* `window.analytics.enableUncaughtExceptionReporting(Enable, success, error)` where Enable is boolean

#Installing Without the CLI <a name="nocli"></a>
Copy the files manually into your project and add the following to your config.xml files:
```xml
<feature name="UniversalAnalytics">
  <param name="ios-package" value="UniversalAnalyticsPlugin" />
</feature>
```
```xml
<feature name="UniversalAnalytics">
  <param name="android-package" value="com.danielcwilson.plugins.analytics.UniversalAnalyticsPlugin" />
</feature>
```
<a name="sdk-files"></a>
You also will need to manually add the Google Analytics SDK files:
* Download the Google Analytics SDK 3.0 for [iOS](https://developers.google.com/analytics/devguides/collection/ios/) and/or [Android](https://developers.google.com/analytics/devguides/collection/android/)
* For iOS, add the downloaded Google Analytics SDK header files and libraries according to the [Getting Started](https://developers.google.com/analytics/devguides/collection/ios/v3) documentation
* For Android, add `libGoogleAnalyticsServices.jar` to your Cordova Android project's `/libs` directory and build path
